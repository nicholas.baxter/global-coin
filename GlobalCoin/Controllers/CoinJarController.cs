﻿using GlobalCoin.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GlobalCoin.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class CoinJarController : ControllerBase
    {
        private readonly ILogger<CoinJarController> _logger;
        private readonly ICoinJar _coinJar;

        public CoinJarController(ILogger<CoinJarController> logger, ICoinJar coinJar)
        {
            _logger = logger;
            _coinJar = coinJar;
        }

        [HttpPost]
        public void Coin(Coin coin)
        {
            _coinJar.AddCoin(coin);
        }

        [HttpGet]
        public decimal Total()
        {
            return _coinJar.GetTotalAmount();
        }

        [HttpPost]
        public void Reset()
        {
            _coinJar.Reset();
        }
    }
}
