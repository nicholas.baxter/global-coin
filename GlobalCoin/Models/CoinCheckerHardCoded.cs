﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GlobalCoin.Models
{
    /// <summary>
    /// Coin volumes in milliletres, calculated from radius and thickness
    /// https://en.wikipedia.org/wiki/Coins_of_the_United_States_dollar#:~:text=Today%2C%20circulating%20coins%20exist%20in,and%20platinum)%20and%20commemorative%20coins.
    /// </summary>
    public class CoinCheckerHardCoded : ICoinChecker
    {
        public bool IsValidUSCoinage(ICoin coin)
        {
            switch (coin.Amount)
            {
                case 0.01M:
                    if (coin.Volume == 1.73294M) 
                        return true;
                    return false;
                case 0.05M:
                    if (coin.Volume == 2.75592M)
                        return true;
                    return false;
                case 0.1M:
                    if (coin.Volume == 1.36043M)
                        return true;
                    return false;
                case 0.25M:
                    if (coin.Volume == 3.23571M)
                        return true;
                    return false;
                case 0.5M:
                    if (coin.Volume == 6.32871M)
                        return true;
                    return false;
                case 1M:
                    if (coin.Volume == 11.76575M)
                        return true;
                    return false;
                default:
                    return false;

            }
        }
    }
}
