﻿namespace GlobalCoin.Models
{
    public interface ICoinChecker
    {
        bool IsValidUSCoinage(ICoin coin);
    }
}