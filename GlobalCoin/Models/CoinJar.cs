﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GlobalCoin.Models
{
    public class CoinJar : ICoinJar
    {
        //dependancies
        private ICoinChecker _coinChecker;

        //fields
        private List<ICoin> coins;
        private decimal currentVolume => coins.Sum(c => c.Volume);
        private decimal totalAmount => coins.Sum(c => c.Amount);

        //constants
        private const decimal MaxVolume = 1242.09M; //42 fluid ounces in milliliters

        public CoinJar(ICoinChecker coinChecker)
        {
            _coinChecker = coinChecker;
            coins = new List<ICoin>();
        }

        public void AddCoin(ICoin coin)
        {
            if (_coinChecker.IsValidUSCoinage(coin) && ThereIsSpace(coin))
            {
                coins.Add(coin);
            }
        }

        private bool ThereIsSpace(ICoin coin)
        {
            return MaxVolume - currentVolume > coin.Volume;
        }

        public decimal GetTotalAmount()
        {
            return totalAmount;
        }

        public void Reset()
        {
            coins = new List<ICoin>();
        }
    }
}
