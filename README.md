# Global Coin

## Assumptions

Do not return any indication for success/failure when adding a coin via the API endpoint, POST CoinJar/Coin, as ICoinJar AddCoin method only returns void, i.e. provides no feedback

Although it would have made sense to have a class for each coin in US circulation (cent, nickel, dime etc.), inheriting from ICoin, this approach was not taken; since the ICoin interface was specified to have get{} and set{} on each property, this approach no longer made sense, as each coin would only need a get{} for their Amount and Volume. So it was assumed the API and ICoinJar object would receive any coin of any Amount and Volume which would then be validated before being added to the CoinJar.

The volume of the Jar would be occupied by each coin perfecty with no empty space between coins.

Hardcoding the coin Amounts and matching Volumes was enough to validate each coin for acceptance to the CoinJar


## Setup

Pull and load solution in Visual Studio, run the API

Import the file "GlobalCoin.postman_collection.json" into the Postman application (install from web). This will create a collection of example HTTP requests to the API, including a POST for each coin in circulation, for your testing.